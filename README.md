# Projet Express Typescript Typeorm



## How To Use
1. Run `npm i` command
2. Create a database
3. Modify or create DATABASE_URL in .env
4. Run `npm start` command
5. Run `npm test` for tests (create fixtures if needed)
