import { Router } from "express";
import { getRepository } from "typeorm";
import { Commandes } from "../entity/Commandes";
import { LigneCommande } from "../entity/LigneCommande";
import { Produit } from "../entity/Produit";

export const cartRouter = Router();

cartRouter.post('/:id', async (req, res) => {
    try {

        let product= await getRepository(Produit).findOne(req.params.id)

        let line = await getRepository(LigneCommande).findOne({where:{user:{id:req.user.id}, produit:{id:req.params.id}}})

        if(line){
            await getRepository(LigneCommande).save({...line, quantity:line.quantity+1})
            return res.status(201).end()
        } 
        let newLine = new LigneCommande;
        newLine = {
            price: product.price,
            quantity: 1,
            produit: product,
            commande: null,
            user: req.user
        }
        await getRepository(LigneCommande).save(newLine)
        return res.status(201).end()



    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }

});


cartRouter.patch('/minus/:id', async (req, res)=>{
    try {        
        

        let line = await getRepository(LigneCommande).findOne({where:{id:req.params.id}})
        
        if(line && line.quantity>1){
            await getRepository(LigneCommande).save({...line, quantity:line.quantity-1})
            return res.status(201).end()
        } return res.status(401).end()
        



    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

cartRouter.patch('/:userid/validate', async (req, res) => {
    if (req.user.id === Number(req.params.userid)) {
        try {
            if (!req.body.address) {
                return res.status(422).json({error: 'You need an address'})
            }
            let lines = await getRepository(LigneCommande).find({ where: { user:{id: req.params.userid} } })
    
            if (!lines) {
                return res.status(404).json({error: 'You have nothing in your cart'})
            }
    
            let commande = new Commandes();
            commande.line = lines;
            commande.total = req.body.total
            commande.date = new Date();
            commande.address = req.body.address
    
            let result = await getRepository(Commandes).save(commande);
    
            getRepository(LigneCommande).createQueryBuilder()
                .update()
                .set({ user: null })
                .where({user:{id: req.params.userid}})
                .execute();
    
            return res.status(201).json(result)
        } catch (error) {
            console.log(error);
            return res.status(500).json(error);
        }
    }
    return res.status(401).end()


})

cartRouter.delete('/:id', async (req,res)=>{
    let line = await getRepository(LigneCommande).findOne({where:{id:req.params.id}, relations:['user']})
    
    if (line && (req.user.id === line.user.id) ) {
        await getRepository(LigneCommande).delete({id:Number(req.params.id)})
        return res.status(202).end()
    } return res.status(404).end()
})