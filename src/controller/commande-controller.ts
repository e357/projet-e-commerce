import { Router } from "express";
import passport from "passport";
import { getRepository } from "typeorm";
import { Commandes } from "../entity/Commandes";
import { patchCommande, validatorCommande } from "../utils/commande-validator";

export const commandeController = Router();

commandeController.get('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    if (req.user.role === "admin" || req.user.id === Number(req.params.id)) {
        try {
            const commandes = await getRepository(Commandes).find({ where: { user: { id: req.params.id } }, relations: ['user'] });
            if (!commandes) {
                return res.status(404).json('commandes introuvables')
            }
            res.json(commandes)
        } catch (error) {
            console.log(error);
            res.status(500).json(error);
        }
    } return res.status(401).json({ message: "vous n'avez pas les droits nécessaires" })

})



commandeController.get('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        const commande = await getRepository(Commandes).findOne(req.params.id, { relations: ['user', 'line'] });
        if (!commande) {
            res.status(404).end();
            return;
        }
        if (req.user.role === "admin" || req.user.id === commande.user.id) {
            res.json(commande)
        }
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
}

)

commandeController.patch('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        let val = patchCommande(req.body);

        if (val != true) {
            return res.status(422).json(val.details[0].message);
        } else {
            const commande = await getRepository(Commandes).findOne(req.params.id);
            if (!commande) {
                return res.status(404).end();
            }

            if (req.user.role === "admin") {
                Object.assign(commande, req.body);
                getRepository(Commandes).save(commande);
                res.json(commande)
            }
        }

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
}

)

commandeController.delete('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        const exists = await getRepository(Commandes).findOne(req.params.id)
        if (exists) {
            if (req.user.role === "admin" || req.user.id === exists.user.id) {
                await getRepository(Commandes).delete(req.params.id);
                return res.status(204).end();
            } return res.status(401).json({ message: "vous n'avez pas les droits nécessaires" })

        } return res.status(404).end()

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
}

)