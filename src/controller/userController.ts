import { Router } from "express";
import { getRepository } from "typeorm";
import { User } from "../entity/User";
import { validatorPatchUser, validatorUser } from "../utils/User-validator";
import bcrypt from 'bcrypt'
import { generateToken } from "../utils/token";
import passport from "passport";
import { createAvatar } from "../utils/uploader";
import multer from "multer";

export const userRoute = Router();

userRoute.get('/account', passport.authenticate('jwt', { session: false }), async (req, res) => {
    let user = await getRepository(User).findOne(req.user.id, { relations: ["commandes", "panier", "panier.produit"] })
    res.json(user)
})


userRoute.patch('/password/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    if (req.user.role == "Admin" || req.user.id == Number(req.params.id)) {
        try {
            let oldPwd = await getRepository(User).findOne(req.params.id);
            let check = await bcrypt.compare(req.body.oldPassword, oldPwd.password)
            
            if (check) {
                let newPwd = await bcrypt.hash(req.body.password, 11);
                req.body.password = newPwd
                let newUser = await getRepository(User).save(req.body);
                return res.status(201).end() 
            }
            return res.status(401).json({error:'Mot de passe incorrect'})
        } catch (error) {
            console.log(error);
            res.status(500).json(error);
        }
    }
})

userRoute.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
    if (req.user.role == 'Admin') {
        try {
            let users = await getRepository(User).find({ relations: ["commandes"] })

            if (users) {
                return res.json(users)
            } return res.status(404).json({ message: "Aucun utilisateurs" })
        } catch (error) {
            console.log(error);
            res.status(500).json(error);
        }
    } return res.status(401).json({ message: "Vous n'avez pas les droits nécessaires" })

})

userRoute.get('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    if (req.user.role == "Admin" || req.user.id == Number(req.params.id)) {
        try {
            let user = await getRepository(User).findOne(req.params.id);
            if (user) {
                return res.json(user)
            } return res.status(404).json({ message: 'Utilisateur inexistant' })

        } catch (error) {
            console.log(error);
            res.status(400).json(error);
        }
    } return res.status(401).json({ message: "vous n'avez pas les droits nécessaires" })
});

userRoute.post('/login', async (req, res) => {
    try {
        const user = await getRepository(User).findOne({ email: req.body.email},{relations: ['commandes', "panier", "panier.produit"]});
        if (user) {
            let samePWD = await bcrypt.compare(req.body.password, user.password)
            if (samePWD) {
                return res.json({
                    user,
                    token: generateToken({
                        id: user.id,
                        email: user.email,
                        role: user.role
                    })
                });
            }
            res.status(401).json({ error: 'Mot de passe incorrect' });
            return
        } res.status(404).json({ error: 'Utilisateur inexistant' })
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

userRoute.post('/register', multer({ storage: multer.memoryStorage() }).single("avatar"), async (req, res) => {
    try {
        console.log(req.file);
        
        let val = validatorUser(req.body);

        if (val != true) {
            return res.status(422).json({error:val.details[0].message});
        } else {
            const newUser = new User;
            Object.assign(newUser, req.body);
            const exists = await getRepository(User).findOne({ email: newUser.email });

            if (exists) {

                res.status(401).json({ error: 'Email déjà utilisé' });

                return;
            }
            if (req.file) {
                newUser.avatar = await createAvatar(req.file)
            }
            newUser.role = 'user';
            newUser.password = await bcrypt.hash(newUser.password, 11);

            await getRepository(User).save(newUser);
            res.status(201).json({
                user: newUser,
                token: generateToken({
                    email: newUser.email,
                    name: newUser.name,
                    id: newUser.id,
                    role: newUser.role
                })
            });

        }
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

userRoute.delete('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    if (req.user.role == 'Admin' || req.user.id == Number(req.params.id)) {
        try {
            let user = await getRepository(User).delete(req.params.id);

            if (user.affected !== 1) {
                return res.status(404).json({ message: 'Utilisateur inexistant' })
            }
            return res.status(204).end()

        }
        catch (error) {
            console.log(error);
            res.status(400).json(error);
        }
    } return res.status(401).json({ message: "Vous n'avez pas les droits nécessaires" })
})

userRoute.patch('/:id', passport.authenticate('jwt', { session: false }), multer({ storage: multer.memoryStorage() }).single("avatar"), async (req, res) => {
    let role = req.user.role

    if (role == "admin" || req.user.id == Number(req.params.id)) {
        try {
            let setRole: string;

            let data = await getRepository(User).findOne(req.params.id);
            if (!data) {
                return res.status(404).json({ error: "Utilisateur inexistant" });
            }

            role != "admin" ? setRole = "user" : setRole = data.role;

            let user = new User();
            if (req.file) {
                data.avatar = await createAvatar(req.file)
            }
            Object.assign(user, { ...{ ...data, ...req.body }, role: setRole })
            user.password = await bcrypt.hash(user.password, 11);
            let val = validatorPatchUser(user);

            if (val != true) {
                return res.status(422).json(val.details[0].message);
            } else {

                await getRepository(User).save(user);
                res.status(202).json(user)
            }

        } catch (error) {
            console.log(error);
            res.status(500).json(error);
        }
    } else {
        res.status(401).json({ error: "Vous n'avez pas les droits nécessaires" });
    }
});

