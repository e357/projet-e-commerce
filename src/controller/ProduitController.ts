import { Router } from "express";
import multer from "multer";
import passport from "passport";
import { getRepository, Like } from "typeorm";
import { Comments } from "../entity/Comments";
import { Produit } from "../entity/Produit";
import { Rating } from "../entity/Rating";
import { validatorComments } from "../utils/comments-validator";
import { validatorPatchProduit, validatorProduit } from "../utils/produit-validator";
import { validatorPatchRating, validatorRating } from "../utils/rating-validator";
import { createThumbnail, uploader } from "../utils/uploader";




export const produitController = Router()

//to get all products with/out relations
produitController.get('/', async (req, res) => {
    try {
        let product =
            !req.query.text ? await getRepository(Produit).find({ relations: ['rating'] }) : await getRepository(Produit).find({ where: { name: Like(`%${req.query.text}%`) } });
        if (product.length > 0) {
            return res.json(product);

        } return res.status(404).json({ message: "Aucun article disponible" })

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    };
});
//get the last 3 products
produitController.get('/last', async (req, res) => {
    try {
        const product = await getRepository(Produit).find({ order: { id: "DESC" }, take: 3 })

        if (product.length < 1) {
            console.log(product);

            res.status(404).end()
            return
        }
        res.json(product);


    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})



//get a product by ID
produitController.get('/:id', async (req, res) => {
    try {
        const product = await getRepository(Produit).findOne(req.params.id, ({ relations: ['rating', 'comments', 'comments.user'] }))
        if (!product) {
            res.status(404).end()
            return
        }
        res.json(product);

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    };
});



//to insert a new product
produitController.post('/', passport.authenticate('jwt', { session: false }), uploader.single("picture"), async (req, res) => {

    if (req.user.role == 'admin') {
        console.log('test');
        
        try {
            //see if it passes the validation before sending
            let val = validatorProduit(req.body);

            if (val != true) {
                //if it doesnt pass, send error message
                return res.status(422).json(val.details[0].message);
            } else {
                //assign as a new product

                let product = new Produit();
                Object.assign(product, req.body);
                //to add the picture as thumbnail
                if (req.file) {
                    
                    createThumbnail(req.file)
                    product.picture = req.file.filename;
                }
                //to save the product
                await getRepository(Produit).save(product);
                return res.status(201).json(product);
            }

        } catch (error) {
            console.log(error);
            return res.status(500).json(error);
        };
    }
    return res.status(403).json({ error: "Vous n'avez pas les droits nécessaires" });

});



//to delete a product by id
produitController.delete('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {

    if (req.user.role == 'admin') {

        try {
            const repo = getRepository(Produit);
            const result = await repo.delete(req.params.id);
            if (result.affected !== 1) {
                res.status(404).end();
                return;
            }
            res.status(204).end();

        } catch (error) {
            console.log(error);
            res.status(500).json(error);
        };
    } else {
        res.status(403).json({ error: "Vous n'avez pas les droits nécessaires" });
    };
});


//to update/change info of a product
produitController.patch('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {

    if (req.user.role == 'admin') {

        try {
            let val = validatorPatchProduit(req.body);

            if (val != true) {
                return res.status(422).json(val.details[0].message);
            } else {
                const repo = getRepository(Produit);
                const product = await repo.findOne(req.params.id);

                if (!product) {
                    res.status(404).end();
                    return;
                }
                Object.assign(product, req.body);
                repo.save(product);
                res.json(product);
            }

        } catch (error) {
            console.log(error);
            res.status(500).json(error);
        };
    } else {
        res.status(403).json({ error: "Vous n'avez pas les droits nécessaires" });
    };
});




//Make controller for post comment on a product
//controller to post a comment
produitController.post('/:id/comment', passport.authenticate('jwt', { session: false }), async (req, res) => {

    try {
        let val = validatorComments(req.body)
        if (val != true) {
            return res.status(422).json(val.details[0].message);
        } else {
            const repo = getRepository(Produit);
            const product = await repo.findOne(req.params.id);

            if (!product) {
                res.status(404).end();
                return;
            }
            let comment = new Comments()

            Object.assign(comment, req.body)
            comment.user = req.user

            comment.produit = product

            await getRepository(Comments).save(comment)

            res.status(201).json(comment);

        }
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

//controller to delete a product
produitController.delete('/:id/comment', passport.authenticate('jwt', { session: false }), async (req, res) => {

    try {
        const exists = await getRepository(Comments).findOne(req.params.id)
        if (exists) {
            if (req.user.role === "admin" || req.user.id === exists.user.id) {
                await getRepository(Comments).delete(req.params.id);
                return res.status(204).end()
            } return res.status(401).json({ message: "vous n'avez pas les droits nécessaires" })
        } return res.status(404).end()

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

//Make controller for post or modify a rating

//controller to rate a product
produitController.post('/:id/rating', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        let val = validatorRating(req.body)
        if (val != true) {
            return res.status(422).json(val.details[0].message);
        } else {
            const repo = getRepository(Produit);
            const product = await repo.findOne(req.params.id);

            if (!product) {
                res.status(404).end();
                return;
            }

            let rating = new Rating()

            Object.assign(rating, req.body)

            rating.produit = product
            rating.user = req.user

            await getRepository(Rating).save(rating)

            res.status(201).json(rating)
        }

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

//controller to modify rating
produitController.patch('/:id/rating', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        let val = validatorPatchRating(req.body)
        if (val != true) {
            return res.status(422).json(val.details[0].message);
        } else {
            const repo = getRepository(Produit);
            const product = await repo.findOne(req.params.id);

            if (!product) {
                res.status(404).end();
                return;
            }
            let rating = new Rating()

            Object.assign(rating, req.body)

            rating.produit = product
            rating.user = req.user

            await getRepository(Rating).save(rating)

            res.status(201).json(rating)
        }

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

