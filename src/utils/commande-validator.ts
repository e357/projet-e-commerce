import Joi from 'joi';

export function validatorCommande(data: Object) {
    let schema = Joi.object({
        total: Joi.number()
            .required(),

        address: Joi.string()
            .required(),

        date: Joi.date()
            .required(),

        user: Joi.object()
        .required(),

        line: Joi.array()
        .required()
    })

    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    const { error, value } = schema.validate(data, options);

    if (error) {
        console.log(error);

        return error;
    } else {
        return true
    }
};

export function patchCommande(data: Object) {
    let schema = Joi.object({
        id: Joi.number()
            .required(),

        total: Joi.number(),

        adress: Joi.string(),

        user: Joi.object(),

        line: Joi.array(),
    })

    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    const { error, value } = schema.validate(data, options);

    if (error) {
        console.log(error);

        return error;
    } else {
        return true
    }
}