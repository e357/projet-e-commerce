import Joi from 'joi';
export function validatorProduit(data:Object) {
        let schema = Joi.object({
            name: Joi.string()
            .min(1)
            .max(100)
            .required(),

            category: Joi.string()
            .required(),

            price: Joi.number()
            .required(),

            stock: Joi.number()
            .required(),

            description: Joi.string()
            .required(),

            rating: Joi.array(),

            comments: Joi.array(),

            commandLine: Joi.array()
        })
    
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    const { error, value } = schema.validate(data, options);

    if (error) {
        return error;
    } else {
        return true
    }
};

export function validatorPatchProduit(data:Object) {
    let schema = Joi.object({

        name: Joi.string(),

        category: Joi.string(),

        price: Joi.number(),

        stock: Joi.number(),

        description: Joi.string(),

        rating: Joi.array(),

        comments: Joi.array(),

        commandLine: Joi.array()
    })

const options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true // remove unknown props
};

const { error, value } = schema.validate(data, options);

if (error) {
    return error;
} else {
    return true
}
};