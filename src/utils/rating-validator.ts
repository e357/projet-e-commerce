//make a validator for rating

import Joi from 'joi';
import { Produit } from '../entity/Produit';
export function validatorRating(data: Object) {
    let schema = Joi.object({
        note: Joi.number()
            .required(),

        user: Joi.object(),

        produit: Joi.object()
    })

    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    const { error, value } = schema.validate(data, options);

    if (error) {
        return error;
    } else {
        return true
    }
};


export function validatorPatchRating(data: Object) {
    let schema = Joi.object({
        id: Joi.number()
            .required(),

        note: Joi.number()
            .required(),

        user: Joi.object(),

        produit: Joi.object()
    })

    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    const { error, value } = schema.validate(data, options);

    if (error) {
        return error;
    } else {
        return true
    }
};