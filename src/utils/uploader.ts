import multer from 'multer';
import fs from 'fs'
import path from 'path';
import sharp from 'sharp';
import { randomUUID } from 'crypto'
//its going to store on the diskstorage, a file on the pc
const storage = multer.diskStorage({
    // where it will be saved '/public/uploads'
    async destination(req, file, cb) {
        const uploadFolder = __dirname + '/../../public/uploads';
        cb(null, uploadFolder);
    },
    //the name of the file and the date
    filename(req, file, cb) {
        //randomUUID <- we can change the file.fieldname by this
        //to generate a random name for the file 
        //needs to be imported
        cb(null, randomUUID() + path.extname(file.originalname))

    }
});



export async function createThumbnail(file: any) {

    const thumbnailFolder = __dirname + '/../../public/uploads/thumbnails/'
    //buffer img stoked on memory
    await sharp(file.path)
        .resize(400, 250, { fit: 'cover' })
        .jpeg({ quality: 90 })
        .toFile(thumbnailFolder + file.filename)

}

export async function createAvatar(file: Express.Multer.File) {

    const thumbnailFolder = __dirname + '/../../public/uploads/avatar/'

    let name = randomUUID() + '.jpeg'
    await sharp(file.buffer,)
        .resize(200, 200, { fit: 'inside' })
        .toFormat('jpeg')
        .jpeg({ quality: 90 })
        .toFile(thumbnailFolder + name)


    return name
}



export const uploader = multer({
    storage, limits: {
        fileSize: 8000000
    }
});
