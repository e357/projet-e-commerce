import Joi from 'joi';
export function validatorUser(data:Object) {
        let schema = Joi.object({
            name: Joi.string()
                .min(1)
                .max(50)
                .required(),

            email: Joi.string()
                .email()
                .required(),

            password: Joi.string()
                .required(),
            
            address: Joi.string()
            .allow(''),

            avatar: Joi.string()
            .allow(''),

            phone: Joi.string()
            .min(10)
            .max(14)
            .pattern(/^[0-9]+$/)
            .allow(''),

            role: Joi.string(),

            commandes : Joi.array(),

            comments: Joi.array(),

            rating: Joi.array()

        })
    
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    const { error, value } = schema.validate(data, options);

    if (error) {
        return error;
    } else {
        return true
    }
};

export function validatorPatchUser(data:Object) {
    let schema = Joi.object({
        id: Joi.number()
        .required(),

        name: Joi.string()
            .min(1)
            .max(50).allow(null),

        email: Joi.string()
            .email().allow(null),

        password: Joi.string().allow(null),
        
        address : Joi.string().allow(null),

        avatar: Joi.string().allow(null),

        phone: Joi.string()
        .min(10)
        .max(14)
        .pattern(/^[0-9]+$/).allow(null),

        role: Joi.string(),

        commandes : Joi.array().allow(null),

        comments: Joi.array().allow(null),

        rating: Joi.array().allow(null)

    })

const options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true // remove unknown props
};

const { error, value } = schema.validate(data, options);

if (error) {
    return error;
} else {
    return true
}
};