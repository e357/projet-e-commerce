
import Joi from 'joi';
export function validatorComments(data: Object) {
    let schema = Joi.object({

        date: Joi.date()
            .required(),

        text: Joi.string()
            .required(),

        user: Joi.object(),

        produit: Joi.object()
    })

    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    const { error, value } = schema.validate(data, options);

    if (error) {
        return error;
    } else {
        return true
    }
};

export function validatorPatchComments(data: Object) {
    let schema = Joi.object({
        id: Joi.number()
            .required(),

        date: Joi.number(),

        text: Joi.string(),

        user: Joi.object(),

        produit: Joi.object()
    })

    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    const { error, value } = schema.validate(data, options);

    if (error) {
        return error;
    } else {
        return true
    }
};