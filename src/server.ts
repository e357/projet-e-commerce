import express from 'express';
import cors from 'cors';
import { configurePassport } from './utils/token';
import passport from 'passport';

import { userRoute } from './controller/userController';

import { produitController } from './controller/ProduitController';

import { commandeController } from './controller/commande-controller';
import { cartRouter } from './controller/cart-controller';

export const server = express();
server.use(cors());

configurePassport();

server.use(express.json())
server.use(passport.initialize());
server.use(express.static('public'));



server.use('/api/user', userRoute)

server.use('/api/produit', produitController)

server.use('/api/cart', passport.authenticate('jwt', {session: false}), cartRouter)

server.use('/api/commandes', commandeController);

