import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Produit } from "./Produit";
import { User } from "./User";

@ Entity()

export class Comments {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({type:'datetime'})
    date: Date;

    @Column({type: "text"})
    text: string;

    @ManyToOne(()=>User, user => user.comments)
    user: User;

    @ManyToOne(()=>Produit, product => product.comments)
    produit: Produit;
}