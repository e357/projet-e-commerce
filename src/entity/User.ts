import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Commandes } from "./Commandes";
import { Comments } from "./Comments";
import { LigneCommande } from "./LigneCommande";
import { Produit } from "./Produit";
import { Rating } from "./Rating";

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    email: string;

    @Column()
    password: string;

    @Column({nullable: true})
    address: string;

    @Column({nullable: true})
    avatar: string;

    @Column({nullable: true})
    phone: string;

    @OneToMany(() => LigneCommande, line => line.user, {nullable: true})
    panier: LigneCommande[];

    @Column({default: "user"})
    role: string 
    @OneToMany(() => Commandes, commande => commande.user, {nullable: true, onDelete: "CASCADE", onUpdate: "CASCADE"})
    commandes: Commandes[];

    @OneToMany(() => Comments, comment => comment.user, {nullable: true})
    comments: Comments[];

    @OneToMany(()=>Rating, rate => rate.user, {nullable: true})
    rating: Rating[];

    toJSON() {
        return {
            id: this.id,
            name: this.name,
            email: this.email,
            role: this.role,
            avatar: this.avatar != 'null'  ? process.env.SERVER_URL+'/uploads/avatar/'+this.avatar : null,
            address: this.address,
            panier: this.panier,
            phone: this.phone,
            commandes: this.commandes,
            comments : this.comments,
            rating: this.rating
        }
    }
}