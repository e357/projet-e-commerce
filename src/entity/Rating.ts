import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Produit } from "./Produit";
import { User } from "./User";

@Entity()

export class Rating {
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    note: number;

    @ManyToOne(()=> User, user => user.rating)
    user: User;

    @ManyToOne(()=>Produit, product => product.rating)
    produit: Produit;
}