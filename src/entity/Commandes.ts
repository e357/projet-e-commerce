import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { LigneCommande } from "./LigneCommande";
import { User } from "./User";

@Entity()
export class Commandes {
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    total:number;

    @Column()
    address: string;

    @Column({type : 'date'})
    date: Date;

    @ManyToOne(()=> User, user => user.commandes)
    user: User;

    @OneToMany(()=>LigneCommande, line => line.commande, {onDelete: "CASCADE", onUpdate: "CASCADE"})
    line: LigneCommande[];
}