import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Commandes } from "./Commandes";
import { Produit } from "./Produit";
import { User } from "./User";

@Entity()
export class LigneCommande {
    @PrimaryGeneratedColumn()
    id?:number;

    @Column()
    price:number;

    @Column()
    quantity:number;

    @ManyToOne(()=>Produit, product => product.commandLine)
    produit: Produit;

    @ManyToOne(()=>Commandes, commande => commande.line, {nullable: true})
    commande: Commandes;

    @ManyToOne(()=>User, user => user.panier, {nullable: true})
    user: User
}