import { boolean } from "joi";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Comments } from "./Comments";
import { LigneCommande } from "./LigneCommande";
import { Rating } from "./Rating";




@Entity()
export class Produit{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    category: string;

    @Column()
    price: number;

    @Column()
    stock: number;

    @Column()
    description: string;

    @Column({default:null})
    picture: string;

    @OneToMany(()=>Rating, rating => rating.produit)
    rating: Rating[];

    @OneToMany(()=>Comments, comment => comment.produit)
    comments: Comments[];

    @OneToMany(()=>LigneCommande, commandLine => commandLine.produit)
    commandLine: LigneCommande[];

    @Column({default: true})
    active: Boolean

    toJSON() {
        return {
            ...this,
            picture: this.picture != 'null' ? process.env.SERVER_URL+'/uploads/'+this.picture : null,
            thumbnailPic: this.picture != 'null' ? process.env.SERVER_URL+'/uploads/thumbnails/'+this.picture : null,
        }

    }

}